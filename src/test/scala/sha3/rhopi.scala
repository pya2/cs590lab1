package sha3

import chisel3.iotesters._

class RhoPiModuleTests(c: RhoPiModule) extends PeekPokeTester(c) {
}

class rhopiTester extends ChiselFlatSpec {
  behavior of "RhoPiModule"
  backends foreach {backend =>
    it should s"do the rhopi function in $backend" in {
      Driver(() => new RhoPiModule, backend)(c => new RhoPiModuleTests(c)) should be (true)
    }
  }
}

object rhopiTester extends App {
  Driver.execute(args, () => new RhoPiModule){ c => new RhoPiModuleTests(c) }
}
