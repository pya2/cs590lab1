package sha3

import chisel3.iotesters._

class ChiModuleTests(c: ChiModule) extends PeekPokeTester(c) {
}

class chiTester extends ChiselFlatSpec {
  behavior of "ChiModule"
  backends foreach {backend =>
    it should s"do the chi function in $backend" in {
      Driver(() => new ChiModule, backend)(c => new ChiModuleTests(c)) should be (true)
    }
  }
}

object chiTester extends App {
  Driver.execute(args, () => new ChiModule){ c => new ChiModuleTests(c) }
}
